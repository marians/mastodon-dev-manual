# Background jobs

**Compatibility info:** This information is based on Mastodon v3.4.

Mastodon uses [Sidekiq](https://sidekiq.org/) to process asynchronous jobs, e. g. sending a status (message, toot) to an instance, or pullig statuses from an account a user follows.

Sidekiq documentation is located in a [GitHub Wiki](https://github.com/mperham/sidekiq/wiki).

## Database

Sidekiq uses Redis as its database.

## Queues

Mastodon maintains several queues for different types of jobs:

- `default`

- `push`

- `mailers`

- `pull`

- `scheduler`: Recurring jobs (similar to cronjobs)

## Jobs in Mastodon code

Workers reside in the [`/app/workers`](https://github.com/mastodon/mastodon/tree/v3.4.6/app/workers) directory.

As an example, the [RedownloadMediaWorker](https://github.com/mastodon/mastodon/blob/v3.4.6/app/workers/redownload_media_worker.rb).

## Recurring jobs

Mastodon has a bunch of recurring jobs configured.

Sidekiq allows to manually deactivate single jobs via the web interface.

TODO: Find out whether there is another way to disable a job.

Some recurring jobs:

- `backup_cleanup_scheduler`
- `doorkeeper_cleanup_scheduler`
- `email_scheduler`
- `feed_cleanup_scheduler`
- `follow_recommendations_scheduler`
- `instance_refresh_scheduler`
- `ip_cleanup_scheduler`
- `media_cleanup_scheduler`
- `pghero_scheduler`
- `scheduled_statuses_scheduler`
- `trending_tags_scheduler`
- `user_cleanup_scheduler`

