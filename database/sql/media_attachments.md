# Table `media_attachments`

**Compatibility notice:** This information is based on Mastodon v3.4

Links: [Source](https://github.com/mastodon/mastodon/blob/v3.4.7/db/schema.rb#L507-L534)

## Description

The `media_attachments` table stores metadata on media that has been used in toots. This can be images, video, audio.

TODO: is there media in here which is not used in toots?

Media can either be stored locally on the instance, or on a remote instance.

## Fields

### `id`

Primary key, defaulted by the [`timestamp_id`](function_timestamp_id.md) function.

### `status_id`

Identifier of the toot the entry belongs to.

### `file_file_name`

If there is a local copy of the file, this field's value is the file name of the local copy.

The copy is stored in 

### `file_content_type`

Media type of the attachment, e. g. `video/mp4` or `image/png`.

### `file_file_size`

Attachment size in bytes.

### `file_updated_at`

Update timestamp.

TODO: How is this different from `updated_at`?

### `remote_url`

For attachments originally published on remote instances, this fields containts the media URL.

For files published originally on the local instance, this field is empty.

### `created_at`

Creation timestamp

### `updated_at`

Update timestamp.

### `shortcode`

TODO

### `type`

Numeric type indicator. Possible values:

- 0: TODO
- 1: TODO
- 2: TODO
- 3: TODO
- 4: TODO

### `file_meta`

TODO

### `account_id`

TODO

### `description`

TODO

### `scheduled_status_id`

TODO

### `blurhash`

TODO

### `processing`

TODO

### `file_storage_schema_version`

TODO

### `thumbnail_file_name`

TODO

### `thumbnail_content_type`

TODO

### `thumbnail_file_size`

TODO

### `thumbnail_updated_at`

TODO

### `thumbnail_remote_url`

TODO

## Sample queries

Bytes consumed by files stored locally

```sql
select sum(file_file_size) from media_attachments where remote_url = '';
```
