# Database

## Tables

- `account_aliases` - Allows to resolve handles like `user@domain` to account IDs and account URLs.

- `accounts` - Accounts in the instance as well as those encountered in the fediverse.

- [`media_attachments`](media_attachments.md) - Metadata for media used in statuses (toots).

- `preview_cards` - Metadata on previews for URLs included in statuses (toots).

- `statuses` - Main table for statuses (toots)

- `users` - Users owning accounts on the instance.

## Functions

- [`timestamp_id`](function_timestamp_id.md)
