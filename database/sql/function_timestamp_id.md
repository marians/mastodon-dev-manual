# `timestamp_id` Function

**Compatibility info:** This information is based on Mastodon v3.4.

Links: [Source](https://github.com/mastodon/mastodon/blob/v3.4.7/lib/mastodon/snowflake.rb)

## Description

The database function `timestamp_id` is used in many database tables to create a numeric primary key.

